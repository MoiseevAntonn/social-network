DROP TABLE IF EXISTS users;

CREATE TABLE users (
    id serial primary key,
    email varchar(255) not null unique,
    name varchar(100) not null,
    secondName varchar(100) not null,
    passwordHash varchar(100) not null,
    status varchar(255),  
    birthday date ,
    city varchar(100),
    mobilePhone varchar(20),
    maritalStatus varchar(20),
    university varchar(50),
    address varchar(50),
);
grant USAGE ,select on sequence users_id_seq to dbuser;
grant ALL privileges on table users to dbuser;

CREATE TABLE  avatars (
    id serial primary key,
    email varchar(255) not null unique,
    avatar bit varying, 
);
grant USAGE ,select on sequence avatars_id_seq to dbuser;
grant ALL privileges on table avatars to dbuser;
