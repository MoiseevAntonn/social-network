import * as React from 'react';
import axios from 'axios';
import { Route, Redirect } from 'react-router-dom';

import LoginForm from "../LoginForm/LoginForm";
import RegistrationForm from "../RegistrationForm";
import Bar from '../Bar/Bar'
import UserProfile from '../UserProfile';
import style from './App.less';
import PrivateRoute from '../PrivateRoute';

export default class App extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			currentUser: null,
		}
	}

	async componentDidMount() {
		const token = localStorage.getItem('token');
		if (token) {
			let response = await fetch('/profile/get', {
				headers: {
					'Authorization': `Bearer ${token}`,
					'Accept': 'application/json'
				}
			});
			if (response.status == 200) {
				const currentUser = await response.json();
				this.setState({ currentUser: currentUser });
				this.props.history.push('/profile');
			}
		}
	}

	render() {
		return <div className={style.rootContainer}>
			<Bar currentUser={this.state.currentUser} signOut={() => { localStorage.removeItem("token"), this.setState({ currentUser: null }) }} />
			<div className={style.app}>
				<Route path='/' exact render={props => <LoginForm {...props} currentUser={this.state.currentUser} setUpUser={(user) => this.setState({ currentUser: user })} />} />
				<Route path='/signup' component={RegistrationForm} />
				<PrivateRoute path='/profile' render={props => <UserProfile {...props} currentUser={this.state.currentUser} />} currentUser={this.state.currentUser} />
			</div>
		</div>;
	}
}
