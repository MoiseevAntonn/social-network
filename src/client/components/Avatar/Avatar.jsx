import * as React from 'react';

export default class Avatar extends React.Component {
    onSubmit(event){
        event.preventDefault();
    }
    render() {
        return (
            <div>
                <img src={'/profile/getAvatar/' + this.props.currentUser.email}/>
                <form onSubmit={this.onSubmit.bind(this)} target='frame' action={'/profile/setAvatar/'+this.props.currentUser.email} method='post' encType="multipart/form-data">
                    <input type='file' id='userAvatar'/>
                    <button>Send</button>
                </form>
            </div>
        )
    }
}