import * as React from 'react';
import { Link } from 'react-router-dom';
import style from './Bar.less'

export default class Bar extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        if (!this.props.currentUser) {
            return (
                <div className={style.bar}>
                    <div>Social network</div>
                    <div>
                        <Link to='/signup'>Sign Up</Link>
                    </div>
                </div>
            );
        } else {
            return (
                    <div className={style.bar}>
                        <div>Social network</div>
                        <div onClick={this.props.signOut}>SignOut</div>
                    </div>
            )
        }
    }
}