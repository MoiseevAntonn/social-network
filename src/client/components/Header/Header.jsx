import * as React from 'react';
import style from './Header.less'

export default class Header extends React.Component{
    constructor(props){
        super(props);
    }

    render(){
        return(
            <div>
                <div className={style.name}>
                    {this.props.currentUser.name}
                </div>
                <div className={style.secondname}>
                    {this.props.currentUser.secondname}
                </div>
            </div>
        );
    }
}