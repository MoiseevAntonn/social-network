import * as React from 'react';
import style from './Button.less';

export default class Button extends React.Component {
    render() {
        const color = this.props.color || 'blue';
        const colorStyle = style[color] || style.blue;
        return <input className={[style.button, colorStyle].join(' ')}
            type='submit'
            value={this.props.value} />;
    }
}