import * as React from 'react';

import commonStyle from '../InputFields.less';

export default class EmailField extends React.Component {
    render() {
        return <input name={this.props.name}
            type='text'
            value={this.props.value}
            onChange={this.props.onChange}
            placeholder={this.props.placeholder || 'Email'}
            className={[commonStyle.inputField].join(' ')}
            />
    }
}