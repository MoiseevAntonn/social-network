import * as React from 'react';

import commonStyle from '../InputFields.less';

export default class PasswordField extends React.Component {
    render() {
        return <input name={this.props.name}
            type='password'
            value={this.props.value}
            onChange={this.props.onChange}
            placeholder={this.props.placeholder || 'Password'}
            className={[commonStyle.inputField].join(' ')}
            />
    }
}