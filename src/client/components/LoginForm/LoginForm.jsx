import * as React from 'react';
import axios from 'axios'
import { Redirect } from 'react-router-dom';

import EmailField from '../InputFields/EmailField/EmailField';
import PasswordField from '../InputFields/PasswordField/PasswordField';
import Button from '../InputFields/Button/Button';

import style from './LoginForm.less';

export default class LoginForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = { email: '', password: '' };
    }
    async onSubmit(event) {
        event.preventDefault();

        try {
            const response = await axios.post('/users/login', {
                email: this.state.email,
                password: this.state.password
            });
            const token = response.data.token;
            if (token) {
                localStorage.setItem("token", response.data.token);
                let res = await fetch('/profile/get', {
                    headers: {
                        'Authorization': `Bearer ${token}`,
                        'Accept': 'application/json'
                    }
                });
                if (res.status == 200) {
                    const currentUser = await res.json();
                    this.props.setUpUser(currentUser);
                }

            }
            this.props.history.push('/profile');

        } catch (error) {
            console.log(error);
        }
    }
    onPasswordChange(event) {
        this.setState({ password: event.target.value });
    }
    onEmailChange(event) {
        this.setState({ email: event.target.value });
    }
    render() {
        if (this.props.currentUser) {
            return <Redirect to='/profile' />;
        }

        return <form onSubmit={this.onSubmit.bind(this)}
            className={style.loginForm}>
            <EmailField name='email'
                value={this.state.email}
                onChange={this.onEmailChange.bind(this)} />
            <PasswordField name='password'
                value={this.state.password}
                onChange={this.onPasswordChange.bind(this)} />
            <div className={style.buttonsContainer}>
                <Button value='Sign in' />
                <Button value='Register' color='white' />
            </div>
        </form>;
    }
}