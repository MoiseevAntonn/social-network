import * as React from 'react';
import { Route, Redirect } from 'react-router-dom';

export default ({ currentUser, render, ...rest }) => (
	<Route
		{...rest}
		render={
			currentUser != null ? (render):((props) => <Redirect to={{
					pathname: "/",
					state: { from: props.location }
				}}
		/>)}
		
	/>
);


