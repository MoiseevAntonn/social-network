import React, { Component } from "react"
import axios from "axios"
import FromError, { FormErrors } from "./FormErrors.jsx"
import { Link,Redirect } from 'react-router-dom';

class RegistrationForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: "",
            password: "",
            firstname: "",
            surname: "",
            formErrors: { email: "", password: "", response: "" },
            emailValid: false,
            passwordValid: false,
            formValid: false

        };
    }
    onSubmitEvent(event) {
        event.preventDefault()
    }
    onNameChange(event) {
        this.setState({ firstname: event.target.value })
    }
    onSecondNameChange(event) {
        this.setState({ surname: event.target.value })
    }
    handleUserEmailPasswordInput(event) {
        const name = event.target.name;
        const value = event.target.value;
        this.setState({ [name]: value }, () => {
            this.validateField(name, value)
        })
    }
    validateField(fieldName, value) {
        let fieldValidationErrors = this.state.formErrors;
        let emailValid = this.state.emailValid;
        let passwordValid = this.state.passwordValid;

        switch (fieldName) {
            case 'email':
                emailValid = value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
                fieldValidationErrors.email = emailValid ? '' : 'email is invalid';
                fieldValidationErrors.response = '';
                break;
            case 'password':
                passwordValid = value.length >= 6;
                fieldValidationErrors.password = passwordValid ? '' : 'too short';
                break;
            default:
                break;
        }
        let formValid = emailValid && passwordValid;
        this.setState({ formErrors: fieldValidationErrors, emailValid: emailValid, passwordValid: passwordValid, formValid: formValid });
    }

    responseErrorHandler(error) {
        let fieldValidationErrors = this.state.formErrors;
        fieldValidationErrors.response = error.message;
        this.setState({ formErrors: fieldValidationErrors });
    }


    render() {
        return (
            <div>
                <form onSubmit={this.onSubmitEvent}>
                    <div>
                        <div>
                            Email
                        </div>
                        <label>
                            <input type="text" name="email" value={this.state.email} onChange={this.handleUserEmailPasswordInput.bind(this)}>
                            </input>
                        </label>
                    </div>
                    <div>
                        <div>
                            FirstName
                        </div>
                        <label>
                            <input type="text" name="firstname" value={this.state.name} onChange={this.onNameChange.bind(this)}>
                            </input>
                        </label>
                    </div>
                    <div>
                        <div>
                            SurName
                        </div>
                        <label>
                            <input type="text" name="surname" value={this.state.secondName} onChange={this.onSecondNameChange.bind(this)}>
                            </input>
                        </label>
                    </div>
                    <div>
                        <div>
                            Password
                        </div>
                        <label>
                            <input type="password" name="password" value={this.state.password} onChange={this.handleUserEmailPasswordInput.bind(this)}>
                            </input>
                        </label>
                    </div>
                    <div>
                        <FormErrors formErrors={this.state.formErrors} />
                    </div>
                    <div>
                        <label>
                            <button type="submit" value="submit" onClick={this.registerUser.bind(this)} disabled={!this.state.formValid}>
                                SignUp
                            </button>
                        </label>
                    </div>
                </form>
                <div>
                    <Link to='/'>Home</Link>
                </div>
            </div>
        )
    }
    registerUser() {
        axios.post("/users/signup", {
            email: this.state.email,
            password: this.state.password,
            name: this.state.firstname,
            secondName: this.state.surname
        })
            .then(
                
            )
            .catch(
                (error) => this.responseErrorHandler(error)
            )

    }
}

export default RegistrationForm;
