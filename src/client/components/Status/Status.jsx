import * as React from 'react';
import axios from 'axios';
import style from './Status.less';

export default class Status extends React.Component {
    constructor(props) {
        super(props);
        this.state = { userStatus: this.props.currentUser.status || "", selectStatus: false }
    }
    onSubmit(event) {
        event.preventDefault();
        this.setState({ selectStatus: !this.state.selectStatus });
        axios.post("/profile/updateStatus", {
            status: this.state.userStatus
        },
            {
                headers: {
                    'Authorization': 'Bearer ' + localStorage.getItem('token'),
                    'Accept': 'application/json'
                }
            }
        )
    }
    onStatusChange(event) {
        this.setState({ userStatus: event.target.value })
    }
    render() {
        if (this.state.selectStatus) {
            return (
                <div>
                    <form onSubmit={this.onSubmit.bind(this)}>
                        <input className={style.statusField} type="text" name="status" value={this.state.userStatus} onChange={this.onStatusChange.bind(this)} />
                        <button className={style.button} type="submit">Edit!</button>
                    </form>
                </div>
            )
        }
        return (
            <div>
                <div className={style.statusField}>
                    {this.state.userStatus}
                </div>
                <button className={style.button} value="edit" onClick={() => this.setState({ selectStatus: true })}>Edit status</button>
            </div>
        )
    }
}