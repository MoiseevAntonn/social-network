import { Component } from 'react';
import * as React from 'react';
import Header from './Header/Header';
import Status from './Status/Status';
import Avatar from './Avatar/Avatar';


export default class UserProfile extends Component {
    constructor(props){
        super(props);
    }
    render() {
        return (
            <div>
                <Header currentUser={this.props.currentUser}/>
                <Status currentUser={this.props.currentUser}/>
                <Avatar currentUser={this.props.currentUser}/>
            </div>
        );
    }
}