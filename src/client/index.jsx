import * as React from 'react';
import * as ReactDOM from 'react-dom';
import App from './components/App/App';
import { BrowserRouter, Route } from 'react-router-dom'
import './index.less';
import axios from 'axios';

if (module.hot) {
    module.hot.accept();
}

ReactDOM.render(<BrowserRouter>
    <Route path='/' component={App} />
</BrowserRouter>,
    document.getElementById('app')
);
