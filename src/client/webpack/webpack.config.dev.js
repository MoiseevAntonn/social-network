const merge = require('webpack-merge');
const config = require('./webpack.config');
const webpack = require('webpack');

module.exports = merge(config, {
    entry: {
        index: [
            './index.jsx',
            'webpack-hot-middleware/client?path=/__webpack_hmr&timeout=20000'
        ]        
    },
    devtool: 'source-map',
    mode: 'development',
    plugins: [new webpack.HotModuleReplacementPlugin()]
})
