const passport = require("passport");
const passportJWT = require("passport-jwt");
const db = require('../db');

const ExtractJWT = passportJWT.ExtractJwt;
const JwtStrategy = passportJWT.Strategy;
const jwtOptions = {};
jwtOptions.jwtFromRequest = ExtractJWT.fromAuthHeaderAsBearerToken();
jwtOptions.secretOrKey = process.env.JWT_KEY;
const strategy = new JwtStrategy(jwtOptions,
    async (jwt_payload, next) => {
        try {
            const user = await db.getUserById(jwt_payload.id);

            next(null, user);
        } catch (error) {
            next(error, null);
        }
    }
);
passport.use(strategy);

module.exports = passport.initialize();