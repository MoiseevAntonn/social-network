const path = require('path');
const webpack = require('../../client/node_modules/webpack');
const devMiddleware = require('../../client/node_modules/webpack-dev-middleware');
const hotMiddleware = require('../../client/node_modules/webpack-hot-middleware');

const webpackConfig = require(path.resolve(__dirname, '../../client/webpack/webpack.config.dev.js'));

webpackConfig.context = path.resolve(__dirname, '../../client');
const compiler = webpack(webpackConfig);

module.exports = [
    devMiddleware(compiler, {
        noInfo: true,
        publicPath: webpackConfig.output.publicPath
    }),
    hotMiddleware(compiler)
];
