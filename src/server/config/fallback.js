const history = require('connect-history-api-fallback');

module.exports = history({
    verbose: process.env.NODE_ENV == 'development',
});