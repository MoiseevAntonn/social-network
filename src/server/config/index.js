const dotenv = require('dotenv');
const path = require('path');

dotenv.config({ path: path.resolve(__dirname, '../.env') });

module.exports = [
    ...(process.env.NODE_ENV == 'development' ? [require('./dev')] : []),
    require('./auth'),
    require('./fallback'),
    require('./misc')
];