const { Pool } = require('pg')

const pool = new Pool();

pool.on('error', (err, client) => {
    console.error(`unexpected error on pg pool: ${err}`);
    process.exit(-1);
});

module.exports = {
    getUserByName: async function (name) {
        const res = await pool.query('SELECT id, name, secondName, email, passwordhash FROM users WHERE name = $1 LIMIT 1', [name]);
        let user = null;
        if (res.rowCount > 0) {
            user = res.rows[0];
        }
        return user;
    },
    getUserByEmail: async function (email) {
        const res = await pool.query('SELECT id, name, secondName, email, passwordhash FROM users WHERE email = $1 LIMIT 1', [email]);
        let user = null;
        if (res.rowCount > 0) {
            user = res.rows[0];
        }
        return user;
    },
    getUserById: async function (id) {
        const res = await pool.query('SELECT * FROM users WHERE id = $1 LIMIT 1', [id]);
        let user = null;
        if (res.rowCount > 0) {
            user = res.rows[0];
        }
        return user;
    },
    createNewUser: async function (user) {
        await pool.query(
            'INSERT INTO users (email, passwordHash, name, secondName) VALUES($1, $2, $3, $4)',
            [user.email, user.passwordHash, user.name, user.secondName]
        );
    },
    updateUserStatus: async function (status, user) {
        await pool.query(
            'UPDATE users SET status = $1 WHERE email = $2',
            [status, user.email]
        );
    },
    saveAvatar: async function (email, avatar) {
        await pool.query(
            'INSERT INTO avatars (email,avatar) VALUES($1, $2) ON CONFLICT (email) DO UPDATE SET avatar = excluded.avatar',
            [email, avatar]
        )
    },
    getAvatar: async function (email) {
        const res = await pool.query('SELECT avatar FROM avatars WHERE email = $1', [email]);
        let avatar = null;
        if (res.rowCount > 0) {
            avatar = res.rows[0];
        }
        return avatar;
    }
}