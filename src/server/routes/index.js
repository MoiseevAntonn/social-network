const router = require('express').Router();

router.use('/users', require('./users'));
router.use('/profile', require('./profile'));

module.exports = router;