const router = require('express').Router();
const passport = require("passport");
const db = require('../../db');

router.get("/get", passport.authenticate('jwt', { session: false }), async function (req, res) {
    const { passwordhash, ...userViewModel } = req.user;
    res.json(userViewModel);
});
router.get('/getAvatar/:email', async function (req, res) {
    try {
        avatar = await db.getAvatar(req.params.email);
        res.send(avatar);
    } catch (error) {
        res.status(500);
    }
})


module.exports = router;