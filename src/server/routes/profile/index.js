const router = require('express').Router();

router.use(require('./get'));
router.use(require('./update'));

module.exports = router;