const router = require('express').Router();
const passport = require("passport");
const db = require('../../db');

router.post("/updateStatus", passport.authenticate('jwt', { session: false }), async function (req, res) {
    if (req.user){
        db.updateUserStatus(req.body.status, req.user);
        res.status(200);
    } else {
        res.status(500);
    }
});
router.post('/setAvatar/:email', async function (req, res) {
    console.log('jhgjhgjhgjhgjh')
    try {
        await db.saveAvatar(req.params.email,req.body);
    } catch (error){
        res.status(500);
    }
})

module.exports = router;