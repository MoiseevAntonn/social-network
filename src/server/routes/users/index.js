const router = require('express').Router();

router.use(require('./login'));
router.use(require('./signup'));

module.exports = router;