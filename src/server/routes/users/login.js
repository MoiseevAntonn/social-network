const db = require('../../db');
const jwt = require("jsonwebtoken");
const bcrypt = require('bcrypt');
const passport = require("passport");

const router = require('express').Router();

router.post("/login", async function (req, res) {
    const { email, password } = req.body;
    if (!email || !password) {
        res.status(400).json({ message: "must specify email and password" });
        return;
    }

    let user;
    try {
        user = await db.getUserByEmail(email);
    } catch (error) {
        res.status(500).json({ message: "unexpected error", error: error });
        return;
    }

    if (!user) {
        res.status(401).json({ message: "invalid username or password" });
        return;
    }

    let result;
    try {
        result = await bcrypt.compare(password, user.passwordhash);
    } catch (error) {
        res.status(500).json({ message: "unexpected error", error: error });
        return;
    }

    if (!result) {
        res.status(401).json({ message: "invalid username or password" });
        return;
    }

    const token = jwt.sign({ id: user.id }, process.env.JWT_KEY);
    res.json({ message: "ok", token: token });
});

module.exports = router;
