const db = require('../../db');
const bcrypt = require('bcrypt');

const router = require('express').Router();

router.post("/signup", async function (req, res) {
    const { email, password, name, secondName } = req.body;
    if (!email || !password || !name || !secondName) {
        res.status(400).json({ message: "must fill required fields" })
        return
    }
    let user;

    try {
        user = await db.getUserByEmail(email);
    } catch (error) {
        res.status(500).json({ message: "unexpected error", error: error })
    }
    if (user) {
        res.status(400).json({ message: "this user already excist" })
        return
    }
    let passwordHash = bcrypt.hashSync(password, bcrypt.genSaltSync(10))

    try {
        db.createNewUser({ email: email, passwordHash: passwordHash, name: name, secondName: secondName })
    } catch (error) {
        res.status(500).json({ message: "unexpected error", error: error })
    }
});

module.exports = router;