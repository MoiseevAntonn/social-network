const express = require('express');

const app = express();

app.use(require('./config'));
app.use(require('./routes'));

const port = process.env.PORT || 3000;
app.listen(port, () => {
    console.log(`App listening on port ${port}`);
    console.log(`Environment: ${process.env.NODE_ENV }`);
});


